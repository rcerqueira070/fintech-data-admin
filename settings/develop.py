#!/usr/bin/env python
# -*- coding: utf-8 -*-
#####################################################
# CONFIGURACION ENTORNO DE DESARROLLO               #
#####################################################
from .common import *

WSGI_APPLICATION = 'settings.wsgi_develop.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fintech_ecosystem',
        'USER': 'postgres',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}