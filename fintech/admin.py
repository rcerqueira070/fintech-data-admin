from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from models import (
    BusinessModel,
    Capability,
    Category,
    Company,
    Groupco,
    Location,
    Partner,
    CompanyCapabilities,
    CompanyBusinessmodel,
    CompanyCategory,
    CompanyGroup,
    CompanyLocation,
    CompanyPartner,
    User,
)



class BusinessModelInline(admin.TabularInline):
    model = CompanyBusinessmodel

class CapabilityModelInline(admin.TabularInline):
    model = CompanyCapabilities

class CategoryModelInline(admin.TabularInline):
    model = CompanyCategory

class GroupModelInline(admin.TabularInline):
    model = CompanyGroup

class LocationModelInline(admin.TabularInline):
    model = CompanyLocation

class PartnerModelInline(admin.TabularInline):
    model = CompanyPartner

class ManytoManys(admin.ModelAdmin):
    inlines = [BusinessModelInline,CategoryModelInline,
               CapabilityModelInline,GroupModelInline,
               LocationModelInline,PartnerModelInline]

admin.site.register(Company,ManytoManys)
admin.site.register(BusinessModel)
admin.site.register(Capability)
admin.site.register(Category)
admin.site.register(Groupco)
admin.site.register(Location)
admin.site.register(Partner)
admin.site.register(User)
