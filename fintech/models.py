from django.db import models
from django.apps import apps

import urllib
import operator

class BusinessModel(models.Model):
    name = models.CharField(unique=True, max_length=50, blank=True, null=True)
    description = models.CharField(max_length=150, blank=True, null=True)
    code = models.CharField(unique=True, max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'business_model'

    def __unicode__(self):
        return self.name

class Capability(models.Model):
    name = models.CharField(unique=True, max_length=200)
    code = models.CharField(unique=True, max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'capability'

    def __unicode__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(unique=True, max_length=200)
    code = models.CharField(unique=True, max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'category'

    def __unicode__(self):
        return self.name



class Groupco(models.Model):
    name = models.CharField(unique=True, max_length=200)
    code = models.CharField(unique=True, max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'groupco'

    def __unicode__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(unique=True, max_length=200)
    geo_location = models.CharField(max_length=200)

    class Meta:
        db_table = 'location'

    def __unicode__(self):
        return self.name

class Partner(models.Model):
    name = models.CharField(unique=True, max_length=200)

    class Meta:
        db_table = 'partner'

    def __unicode__(self):
        return self.name

class Company(models.Model):
    name = models.CharField(unique=True, max_length=200)
    description = models.TextField(blank=True)
    phone = models.CharField(max_length=1500, blank=True, null=True)
    email = models.CharField(max_length=1500, blank=True, null=True)
    website = models.CharField(max_length=1500, blank=True, null=True)
    twitter = models.CharField(max_length=1500, blank=True, null=True)
    linkedin = models.CharField(max_length=1500, blank=True, null=True)
    # logo = models.CharField(max_length=250, blank=True, null=True)
    owner = models.ForeignKey('User', models.DO_NOTHING, blank=True, null=True)
    key_features = models.TextField(blank=True)
    category = models.ManyToManyField(Category, blank=True, 
                                      through='fintech.CompanyCategory')
    capabilities = models.ManyToManyField(Capability, blank=True, 
                                          through='fintech.CompanyCapabilities')
    location = models.ManyToManyField(Location, blank=True, 
                                      through='fintech.CompanyLocation')
    partner = models.ManyToManyField(Partner, blank=True, 
                                     through='fintech.CompanyPartner')
    group = models.ManyToManyField(Groupco, blank=True, 
                                   through='fintech.CompanyGroup')
    businessmodel = models.ManyToManyField(BusinessModel, blank=True, 
                                         through='fintech.CompanyBusinessmodel')


    class Meta:
        db_table = 'company'

    def __unicode__(self):
        return self.name


class CompanyCapabilities(models.Model):
    capability = models.ForeignKey(Capability, models.DO_NOTHING)
    company = models.ForeignKey(Company, models.DO_NOTHING)

    class Meta:
        db_table = 'company_capabilities'

    def __unicode__(self):
        return str(self.capability)

class CompanyBusinessmodel(models.Model):
    business_model = models.ForeignKey(BusinessModel, models.DO_NOTHING)
    company = models.ForeignKey(Company, models.DO_NOTHING)

    class Meta:
        db_table = 'company_businessmodel'

    def __unicode__(self):
        return str(self.business_model.description)

class CompanyCategory(models.Model):
    category = models.ForeignKey(Category, models.DO_NOTHING)
    company = models.ForeignKey(Company, models.DO_NOTHING)

    class Meta:
        db_table = 'company_category'

    def __unicode__(self):
        return str(self.category)

class CompanyGroup(models.Model):
    group = models.ForeignKey('Groupco', models.DO_NOTHING)
    company = models.ForeignKey(Company, models.DO_NOTHING)

    class Meta:
        db_table = 'company_group'

    def __unicode__(self):
        return str(self.group)

class CompanyLocation(models.Model):
    location = models.ForeignKey('Location', models.DO_NOTHING)
    company = models.ForeignKey(Company, models.DO_NOTHING)

    class Meta:
        db_table = 'company_location'

    def __unicode__(self):
        return str(self.location)

class CompanyPartner(models.Model):
    partner = models.ForeignKey('Partner', models.DO_NOTHING)
    company = models.ForeignKey(Company, models.DO_NOTHING)

    class Meta:
        db_table = 'company_partner'

    def __unicode__(self):
        return str(self.partner)

class User(models.Model):
    username = models.CharField(unique=True, max_length=80, 
                                blank=True, null=True)
    email = models.CharField(unique=True, max_length=120, 
                             blank=True, null=True)
    password_hash = models.CharField(max_length=120,
                                     blank=True, null=True)

    class Meta:
        db_table = 'user'

    def __unicode__(self):
        return self.username