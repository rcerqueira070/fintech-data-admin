# README #


### What is this repository for? ###

* For fintech ecosystem data administration.

### How do I get set up? ###

* Summary of set up
    - python need to be installed 
    - install virtualenv

* Configuration
    - make a virtualenv called "finadmin"
    - in order to make some necessary changes in the db run:
        - sh scripts/prod_db_change.sh
        
* Dependencies
    - install with pip
        - pip install -r requirements.txt
	
* Database configuration
    - Change os.environ.setdefault in manage.py from "settings.develop" to "settings.prod" 
    - run: python manage.py migrate
    - create a super user
	- python manage.py createsuperuser
	- username: admin
	- email adress: fintechecosystem@nekso.io
	- password: user_password
	- password (again): user_password

* Runserver
    - Run server with the commando "python manage.py runserver" in the folder where is manage.py 
    - the application runs on http://localhost:8000/admin/

### Who do I talk to? ###

* Repo owner or admin
* ricardoc@blanclabs.com
